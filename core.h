/**
 * Fredrik Olsson, 2002
 * fredrik.g.olsson at gmail.com
 */

#ifndef CHIP8_CORE_H
#define CHIP8_CORE_H

/* 0x0200 on most machines, but ETI-660 starts at 0x0600. */
#define CHIP8_STARTINGPOINT 0x0200

/* Change flag to disable or enable compatibility workaround hacks. */
#define CHIP8_QUIRKS        0

/* All sizes must be powers of two. */
#define CHIP8_MEMORYSIZE    4096
#define CHIP8_RESOLUTION_X  64
#define CHIP8_RESOLUTION_Y  32
#define CHIP8_SCREENSIZE    (CHIP8_RESOLUTION_X * CHIP8_RESOLUTION_Y)
#define CHIP8_STACKSIZE     16

/* Exported core functions. */
void chip8_exec(void);
const unsigned char *chip8_framebuffer(void);
void chip8_init(const unsigned char *rom, unsigned int size);
void chip8_reset(void);

/* Externally implemented functions, platform specific. */
extern void chip8_buzz(void);
extern void chip8_draw(const unsigned char *framebuffer);
extern unsigned char chip8_key(void);
extern unsigned char chip8_random(void);

#endif
