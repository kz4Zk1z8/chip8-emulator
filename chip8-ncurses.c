#include <fcntl.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include <ncurses.h>

#include "core.h"

static unsigned char key;

static int read_keyboard(void)
{
  int res = 0;

  switch (getch())
  {
    case '1': key = 0x01; break;
    case '2': key = 0x02; break;
    case '3': key = 0x03; break;
    case '4': key = 0x0c; break;
    case 'q': key = 0x04; break;
    case 'w': key = 0x05; break;
    case 'e': key = 0x06; break;
    case 'r': key = 0x0d; break;
    case 'a': key = 0x07; break;
    case 's': key = 0x08; break;
    case 'd': key = 0x09; break;
    case 'f': key = 0x0e; break;
    case 'z': key = 0x0a; break;
    case 'x': key = 0x00; break;
    case 'c': key = 0x0b; break;
    case 'v': key = 0x0f; break;
    case 27: res = -1;
    case KEY_BACKSPACE: chip8_reset();
    default: key = 0xf0; break;
  }
  return res;
}

static int run_emulator(const char *arg)
{
  const struct timespec delay = { 0, 16666666 };
  unsigned char mem[CHIP8_MEMORYSIZE - CHIP8_STARTINGPOINT];
  int fd = -1;
  int res = -1;
  ssize_t size = 0;

  if (arg == NULL)
  {
    goto fail;
  }

  fd = open(arg, O_RDONLY);
  if (fd == -1)
  {
    goto fail;
  }

  size = read(fd, mem, sizeof(mem));

  if (close(fd) != 0)
  {
    goto fail;
  }

  if (size < 0)
  {
    goto fail;
  }

  srand((unsigned int) time(NULL));
  chip8_init(mem, (unsigned int) size);
  chip8_reset();

  initscr();
  cbreak();
  curs_set(0);
  keypad(stdscr, TRUE);
  nodelay(stdscr, TRUE);
  noecho();
  ESCDELAY = 0;

  while (read_keyboard() == 0)
  {
    chip8_exec();
    nanosleep(&delay, NULL);
  }

  endwin();
  res = 0;
fail:
  return res;
}

void chip8_buzz(void)
{
}

void chip8_draw(const unsigned char *framebuffer)
{
  int x;
  int y;
  unsigned char c;

  for (y = 0; y < CHIP8_RESOLUTION_Y; ++y)
  {
    for (x = 0; x < CHIP8_RESOLUTION_X; x += 8)
    {
      c = framebuffer[y * CHIP8_RESOLUTION_X / 8 + x / 8];
      move(y, x * CHIP8_RESOLUTION_X);
      printw("%c%c%c%c%c%c%c%c",
             (((c >> 7) & 1) ? '#' : ' '),
             (((c >> 6) & 1) ? '#' : ' '),
             (((c >> 5) & 1) ? '#' : ' '),
             (((c >> 4) & 1) ? '#' : ' '),
             (((c >> 3) & 1) ? '#' : ' '),
             (((c >> 2) & 1) ? '#' : ' '),
             (((c >> 1) & 1) ? '#' : ' '),
             (((c >> 0) & 1) ? '#' : ' '));
    }
  }
  refresh();
}

unsigned char chip8_key(void)
{
  return key;
}

unsigned char chip8_random(void)
{
  return rand() & 0xff;
}

int main(int argc, const char *argv[])
{
  int i;

  for (i = 1; i < argc; ++i)
  {
    if (run_emulator(argv[i]) != 0)
    {
      return EXIT_FAILURE;
    }
  }
  return EXIT_SUCCESS;
}
