#define STRICT
#define WIN32_LEAN_AND_MEAN
#define _CRT_RAND_S

#include <windows.h>
#include <commdlg.h>
#include <shellapi.h>
#include <stdlib.h>
#include <tchar.h>

#include "core.h"

#define APPLICATION_NAME  TEXT("Chip8")
#define EMULATION_SPEED   (1000 / 60)
#define KEY_DOWN(vk_code) (GetAsyncKeyState(vk_code) & 0x8000)
#define PIXEL_SCALE       (8)

static HANDLE hMutex = NULL;
static HWND hWndMain = NULL;

/**
 * Read ROM from file and reset emulator.
 */
static BOOL LoadFile(_In_ LPCTSTR lpFile)
{
  unsigned char buf[CHIP8_MEMORYSIZE - CHIP8_STARTINGPOINT];
  HANDLE hFile;
  DWORD dwLen = 0;
  BOOL bRes = TRUE;

  if (!lpFile || _tcsclen(lpFile) == 0)
  {
    return FALSE;
  }

  hFile = CreateFile(lpFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
  if (hFile != INVALID_HANDLE_VALUE)
  {
    if (!ReadFile(hFile, buf, sizeof(buf), &dwLen, NULL))
    {
      bRes = FALSE;
    }

    if (!CloseHandle(hFile))
    {
      bRes = FALSE;
    }

    if (bRes)
    {
      WaitForSingleObject(hMutex, INFINITE);
      chip8_init(buf, dwLen);
      chip8_reset();
      ReleaseMutex(hMutex);
      return TRUE;
    }
  }

  MessageBox(NULL, TEXT("Load error"), APPLICATION_NAME, MB_OK);
  return FALSE;
}

/**
 * Open file dialog and read file.
 */
static void LoadFileDialog(void)
{
  TCHAR szFile[MAX_PATH];
  OPENFILENAME ofn;

  ZeroMemory(&ofn, sizeof(ofn));
  ofn.lStructSize = sizeof(ofn);
  ofn.lpstrFile = szFile;
  ofn.lpstrFile[0] = TEXT('\0');
  ofn.nMaxFile = MAX_PATH;
  ofn.Flags = OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_PATHMUSTEXIST;

  if (GetOpenFileName(&ofn))
  {
    LoadFile(ofn.lpstrFile);
  }
}

/**
 * Read file from drag-and-drop event.
 */
static void LoadFileDrop(_In_ HDROP hDrop)
{
  TCHAR szFile[MAX_PATH];

  if (DragQueryFile(hDrop, 0, szFile, MAX_PATH))
  {
    LoadFile(szFile);
  }
}

/**
 * Timer callback.
 */
static void CALLBACK NextFrame(_In_ PVOID lpParameter, _In_ BOOLEAN bTimerOrWaitFired)
{
  UNREFERENCED_PARAMETER(lpParameter);
  UNREFERENCED_PARAMETER(bTimerOrWaitFired);

  WaitForSingleObject(hMutex, 0);
  chip8_exec();
  ReleaseMutex(hMutex);
}

/**
 * Process messages.
 */
static LRESULT CALLBACK WindowProc(_In_ HWND hWnd, _In_ UINT uMsg, _In_ WPARAM wParam, _In_ LPARAM lParam)
{
  if (uMsg == WM_KEYDOWN)
  {
    if (wParam == VK_BACK)
    {
      WaitForSingleObject(hMutex, INFINITE);
      chip8_reset();
      ReleaseMutex(hMutex);
      return 0;
    }
    else if (wParam == VK_RETURN)
    {
      LoadFileDialog();
      return 0;
    }
    else if (wParam == VK_ESCAPE)
    {
      DestroyWindow(hWndMain);
      return 0;
    }
  }
  else if (uMsg == WM_DROPFILES)
  {
    LoadFileDrop((HDROP) wParam);
    return 0;
  }
  else if (uMsg == WM_DESTROY)
  {
    PostQuitMessage(0);
    return 0;
  }
  return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

/**
 * Make noise.
 */
void chip8_buzz(void)
{
}

/**
 * Update screen.
 */
void chip8_draw(const unsigned char *framebuffer)
{
  HDC hDC;
  HDC hBufferDC;
  HBITMAP hBmp;
  HGDIOBJ hBmpBuf;
  RECT rect;

  hDC = GetDC(hWndMain);
  hBufferDC = CreateCompatibleDC(hDC);
  hBmp = CreateBitmap(CHIP8_RESOLUTION_X, CHIP8_RESOLUTION_Y, 1, 1, framebuffer);
  hBmpBuf = SelectObject(hBufferDC, hBmp);
  GetClientRect(hWndMain, &rect);
  StretchBlt(hDC, 0, 0, rect.right, rect.bottom, hBufferDC, 0, 0, CHIP8_RESOLUTION_X, CHIP8_RESOLUTION_Y, SRCCOPY);

  ReleaseDC(hWndMain, hDC);
  DeleteDC(hBufferDC);
  DeleteObject(hBmp);
  DeleteObject(hBmpBuf);
}

/**
 * Check for keys.
 */
unsigned char chip8_key(void)
{
  if KEY_DOWN('1') return 0x01;
  if KEY_DOWN('2') return 0x02;
  if KEY_DOWN('3') return 0x03;
  if KEY_DOWN('4') return 0x0c;
  if KEY_DOWN('Q') return 0x04;
  if KEY_DOWN('W') return 0x05;
  if KEY_DOWN('E') return 0x06;
  if KEY_DOWN('R') return 0x0d;
  if KEY_DOWN('A') return 0x07;
  if KEY_DOWN('S') return 0x08;
  if KEY_DOWN('D') return 0x09;
  if KEY_DOWN('F') return 0x0e;
  if KEY_DOWN('Z') return 0x0a;
  if KEY_DOWN('X') return 0x00;
  if KEY_DOWN('C') return 0x0b;
  if KEY_DOWN('V') return 0x0f;
  return 0xf0;
}

/**
 * Get random number.
 */
unsigned char chip8_random(void)
{
  unsigned int n = 0;

  rand_s(&n);
  return (unsigned char) n;
}

/**
 * Main function.
 */
int WINAPI _tWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPTSTR lpCmdLine, _In_ int nCmdShow)
{
  WNDCLASS wc;
  RECT rect;
  MSG msg;
  HANDLE hTimer;

  UNREFERENCED_PARAMETER(hPrevInstance);
  UNREFERENCED_PARAMETER(nCmdShow);

  wc.style = CS_BYTEALIGNCLIENT | CS_OWNDC;
  wc.lpfnWndProc = WindowProc;
  wc.cbClsExtra = 0;
  wc.cbWndExtra = 0;
  wc.hInstance = hInstance;
  wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wc.hCursor = LoadCursor(NULL, IDC_ARROW);
  wc.hbrBackground = (HBRUSH) GetStockObject(NULL_BRUSH);
  wc.lpszMenuName = NULL;
  wc.lpszClassName = APPLICATION_NAME;

  if (!RegisterClass(&wc))
  {
    return 0;
  }

  rect.left = 0;
  rect.top = 0;
  rect.right = CHIP8_RESOLUTION_X * PIXEL_SCALE;
  rect.bottom = CHIP8_RESOLUTION_Y * PIXEL_SCALE;
  if (!AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, FALSE))
  {
    return 0;
  }

  hWndMain = CreateWindowEx(WS_EX_ACCEPTFILES, APPLICATION_NAME, APPLICATION_NAME, WS_OVERLAPPEDWINDOW | WS_VISIBLE, CW_USEDEFAULT, CW_USEDEFAULT, rect.right - rect.left, rect.bottom - rect.top, NULL, NULL, hInstance, NULL);
  if (!hWndMain)
  {
    return 0;
  }

  hMutex = CreateMutex(NULL, FALSE, NULL);
  if (!hMutex)
  {
    return 0;
  }

  if (!LoadFile(lpCmdLine))
  {
    LoadFileDialog();
  }

  if (!CreateTimerQueueTimer(&hTimer, NULL, NextFrame, NULL, 0, EMULATION_SPEED, WT_EXECUTEINTIMERTHREAD))
  {
    return 0;
  }

  while (GetMessage(&msg, NULL, 0, 0))
  {
    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }

  while (!DeleteTimerQueueTimer(NULL, hTimer, INVALID_HANDLE_VALUE))
  {
    if (GetLastError() == ERROR_IO_PENDING)
    {
      break;
    }
  }

  CloseHandle(hMutex);
  UnregisterClass(APPLICATION_NAME, hInstance);

  return (int) msg.wParam;
}
