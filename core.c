#include <string.h>

#include "core.h"

#define MEM_LOAD(x)     (memory[(x) & (sizeof(memory) - 1)])
#define MEM_STORE(x, y) (memory[(x) & (sizeof(memory) - 1)] = (y))

#define OP_X000(x) (((x) & 0xf000) >> 12)
#define OP_0X00(x) (((x) & 0x0f00) >>  8)
#define OP_00X0(x) (((x) & 0x00f0) >>  4)
#define OP_000X(x) (((x) & 0x000f)      )
#define OP_00XX(x) (((x) & 0x00ff)      )
#define OP_0XXX(x) (((x) & 0x0fff)      )

#define FONT_START_8 0x0000
#define FONT_START_S 0x0050

static unsigned int  stack[CHIP8_STACKSIZE];
static unsigned int  I;
static unsigned int  PC;
static unsigned int  SP;
static unsigned int  delay_timer;
static unsigned int  sound_timer;
static unsigned int  video_update;
static unsigned char HP48[8];
static unsigned char V[16];
static unsigned char memory[CHIP8_MEMORYSIZE];
static unsigned char screen[CHIP8_SCREENSIZE / 8];

static void op_0000(const unsigned int opcode)
{
  const unsigned int n   = OP_000X(opcode);
  unsigned int x;
  unsigned int y;

  /* HALT */
  if (opcode == 0x0000)
  {
    PC -= 2;
  }
  /* SCD N */
  else if ((opcode & 0xfff0) == 0x00c0)
  {
    memmove(screen + (CHIP8_RESOLUTION_X / 8) * n, screen,
            sizeof(screen) - (CHIP8_RESOLUTION_X / 8) * n);
    memset(screen, 0, (CHIP8_RESOLUTION_X / 8) * n);
    video_update = 1;
  }
  /* CLS */
  else if (opcode == 0x00e0)
  {
    memset(screen, 0, sizeof(screen));
    video_update = 1;
  }
  /* RET */
  else if (opcode == 0x00ee)
  {
    SP = (SP - 1) & (CHIP8_STACKSIZE - 1);
    PC = stack[SP];
  }
  /* SCR */
  else if (opcode == 0x00fb)
  {
    for (y = 0; y < sizeof(screen); y += CHIP8_RESOLUTION_X / 8)
    {
      for (x = CHIP8_RESOLUTION_X / 8 - 1; x; --x)
      {
        screen[x + y] = (unsigned char) (screen[x + y - 1] << 4 |
                                         screen[x + y] >> 4);
      }
      screen[y] >>= 4;
    }
    video_update = 1;
  }
  /* SCL */
  else if (opcode == 0x00fc)
  {
    for (y = 0; y < sizeof(screen); y += CHIP8_RESOLUTION_X / 8)
    {
      for (x = 0; x < CHIP8_RESOLUTION_X / 8 - 1; ++x)
      {
        screen[x + y] = (unsigned char) (screen[x + y] << 4 |
                                         screen[x + y + 1] >> 4);
      }
      screen[y + CHIP8_RESOLUTION_X / 8 - 1] <<= 4;
    }
    video_update = 1;
  }
  /* EXIT */
  else if (opcode == 0x00fd)
  {
    chip8_reset();
  }
}

static void op_1000(const unsigned int opcode)
{
  const unsigned int nnn = OP_0XXX(opcode);

  /* JP NNN */
  PC = nnn;
}

static void op_2000(const unsigned int opcode)
{
  const unsigned int nnn = OP_0XXX(opcode);

  /* CALL NNN */
  stack[SP] = PC;
  SP = (SP + 1) & (CHIP8_STACKSIZE - 1);
  PC = nnn;
}

static void op_3000(const unsigned int opcode)
{
  const unsigned int x   = OP_0X00(opcode);
  const unsigned int nn  = OP_00XX(opcode);

  /* SE VX, NN */
  if (V[x] == nn)
  {
    PC += 2;
  }
}

static void op_4000(const unsigned int opcode)
{
  const unsigned int x   = OP_0X00(opcode);
  const unsigned int nn  = OP_00XX(opcode);

  /* SNE VX, NN */
  if (V[x] != nn)
  {
    PC += 2;
  }
}

static void op_5000(const unsigned int opcode)
{
  const unsigned int x   = OP_0X00(opcode);
  const unsigned int y   = OP_00X0(opcode);

  /* SE VX, VY */
  if (V[x] == V[y])
  {
    PC += 2;
  }
}

static void op_6000(const unsigned int opcode)
{
  const unsigned int x   = OP_0X00(opcode);
  const unsigned int nn  = OP_00XX(opcode);

  /* LD VX, NN */
  V[x] = (unsigned char) nn;
}

static void op_7000(const unsigned int opcode)
{
  const unsigned int x   = OP_0X00(opcode);
  const unsigned int nn  = OP_00XX(opcode);

  /* ADD VX, NN */
  V[x] += (unsigned char) nn;
}

static void op_8000(const unsigned int opcode)
{
  const unsigned int x   = OP_0X00(opcode);
  const unsigned int y   = OP_00X0(opcode);
  const unsigned int n   = OP_000X(opcode);
  unsigned char carry;

  switch (n)
  {
    /* LD VX, VY */
    case 0x00:
      V[x] = V[y];
      break;
    /* OR VX, VY */
    case 0x01:
      V[x] |= V[y];
      break;
    /* AND VX, VY */
    case 0x02:
      V[x] &= V[y];
      break;
    /* XOR VX, VY */
    case 0x03:
      V[x] ^= V[y];
      break;
    /* ADD VX, VY */
    case 0x04:
      carry = V[x] > 0xff - V[y];
      V[x] += V[y];
      V[15] = carry;
      break;
    /* SUB VX, VY */
    case 0x05:
      carry = V[x] >= V[y];
      V[x] -= V[y];
      V[15] = carry;
      break;
    /* SHR VX */
    case 0x06:
#if CHIP8_QUIRKS
      carry = V[x] & 1;
      V[x] >>= 1;
#else
      carry = V[y] & 1;
      V[x] = V[y] >> 1;
#endif
      V[15] = carry;
      break;
    /* SUBN VX, VY */
    case 0x07:
      carry = V[y] >= V[x];
      V[x] = V[y] - V[x];
      V[15] = carry;
      break;
    /* SHL VX */
    case 0x0e:
#if CHIP8_QUIRKS
      carry = V[x] >> 7;
      V[x] <<= 1;
#else
      carry = V[y] >> 7;
      V[x] = (unsigned char) (V[y] << 1);
#endif
      V[15] = carry;
      break;
  }
}

static void op_9000(const unsigned int opcode)
{
  const unsigned int x   = OP_0X00(opcode);
  const unsigned int y   = OP_00X0(opcode);

  /* SNE VX, VY */
  if (V[x] != V[y])
  {
    PC += 2;
  }
}

static void op_a000(const unsigned int opcode)
{
  const unsigned int nnn = OP_0XXX(opcode);

  /* LD I, NNN */
  I = nnn;
}

static void op_b000(const unsigned int opcode)
{
  const unsigned int nnn = OP_0XXX(opcode);

  /* JP V0, NNN */
  PC = V[0] + nnn;
}

static void op_c000(const unsigned int opcode)
{
  const unsigned int x   = OP_0X00(opcode);
  const unsigned int nn  = OP_00XX(opcode);

  /* RND VX, NN */
  V[x] = chip8_random() & nn;
}

static void op_d000(const unsigned int opcode)
{
  const unsigned int x   = OP_0X00(opcode);
  const unsigned int y   = OP_00X0(opcode);
  const unsigned int n   = OP_000X(opcode);
  unsigned int i;
  unsigned int line;
  unsigned int max;
  unsigned int offset;
  unsigned int sprite;
  unsigned int hpos;
  unsigned int vpos;

  /* DRW VX, VY, N */
  V[15] = 0;
  hpos = V[x] & (CHIP8_RESOLUTION_X - 1);
  vpos = V[y] & (CHIP8_RESOLUTION_Y - 1);

  max = vpos + n < CHIP8_RESOLUTION_Y ? n: CHIP8_RESOLUTION_Y - vpos;
  for (i = 0; i < max; ++i)
  {
    sprite = (unsigned int) MEM_LOAD(I + i) << (8 - (hpos % 8));
    offset = (vpos * CHIP8_RESOLUTION_X + hpos) / 8;
    line = (unsigned int) screen[offset] << 8;
    if (offset < sizeof(screen) - 1)
    {
      line |= screen[offset + 1];
    }
    if (line & sprite)
    {
      V[15] = 1;
    }
    line ^= sprite;
    screen[offset] = (line >> 8) & 0xff;
    if (offset < sizeof(screen) - 1)
    {
      screen[offset + 1] = line & 0xff;
    }
    ++vpos;
  }
  video_update = 1;
}

static void op_e000(const unsigned int opcode)
{
  const unsigned int x   = OP_0X00(opcode);
  const unsigned int nn  = OP_00XX(opcode);
  const unsigned char k  = chip8_key();

  /* SKP VX */
  if (nn == 0x9e && k == V[x])
  {
    PC += 2;
  }
  /* SKNP VX */
  else if (nn == 0xa1 && k != V[x])
  {
    PC += 2;
  }
}

static void op_f000(const unsigned int opcode)
{
  const unsigned int x   = OP_0X00(opcode);
  const unsigned int nn  = OP_00XX(opcode);
  unsigned int i;

  switch (nn)
  {
    /* LD VX, DT */
    case 0x07:
      V[x] = delay_timer & 0xff;
      break;
    /* LD VX, KEY */
    case 0x0a:
      V[x] = chip8_key();
      if (V[x] & 0xf0)
      {
        PC -= 2;
      }
      break;
    /* LD DT, VX */
    case 0x15:
      delay_timer = V[x];
      break;
    /* LD ST, VX */
    case 0x18:
      sound_timer = V[x];
      break;
    /* ADD I, VX */
    case 0x1e:
      I += V[x];
#if CHIP8_QUIRKS
      V[15] = I > 0x0fff;
#endif
      break;
    /* LD LF, VX */
    case 0x29:
      I = FONT_START_8 + V[x] * 5;
      break;
    /* LD HF, VX */
    case 0x30:
      I = FONT_START_S + V[x] * 10;
      break;
    /* LD B, VX */
    case 0x33:
      MEM_STORE(I    , V[x] / 100     );
      MEM_STORE(I + 1, V[x] /  10 % 10);
      MEM_STORE(I + 2, V[x]       % 10);
      break;
    /* LD [I], VX */
    case 0x55:
      for (i = 0; i <= x; ++i)
      {
        MEM_STORE(I + i, V[i]);
      }
#if !CHIP8_QUIRKS
      I += x + 1;
#endif
      break;
    /* LD VX, [I] */
    case 0x65:
      for (i = 0; i <= x; ++i)
      {
        V[i] = MEM_LOAD(I + i);
      }
#if !CHIP8_QUIRKS
      I += x + 1;
#endif
      break;
    /* LD R, VX */
    case 0x75:
      for (i = 0; i <= (x & 7); ++i)
      {
        HP48[i] = V[i];
      }
      break;
    /* LD VX, R */
    case 0x85:
      for (i = 0; i <= (x & 7); ++i)
      {
        V[i] = HP48[i];
      }
      break;
  }
}

void chip8_exec(void)
{
  static void (*const optable[16])(const unsigned int) =
  {
    op_0000, op_1000, op_2000, op_3000, op_4000, op_5000, op_6000, op_7000,
    op_8000, op_9000, op_a000, op_b000, op_c000, op_d000, op_e000, op_f000
  };
  unsigned int ctr = 12;
  unsigned int instr;
  unsigned int opcode;

  do
  {
    opcode = (unsigned int) (MEM_LOAD(PC) << 8) | MEM_LOAD(PC + 1);
    PC = (PC + 2) & (sizeof(memory) - 1);
    instr = OP_X000(opcode);
    optable[instr](opcode);
    --ctr;
  }
  while (ctr);

  if (delay_timer)
  {
    --delay_timer;
  }

  if (sound_timer)
  {
    --sound_timer;
    chip8_buzz();
  }

  if (video_update)
  {
    video_update = 0;
    chip8_draw(screen);
  }
}

const unsigned char *chip8_framebuffer(void)
{
  return screen;
}

void chip8_init(const unsigned char *rom, unsigned int size)
{
  unsigned int i = sizeof(memory) - CHIP8_STARTINGPOINT;

  if (size < i)
  {
    i = size;
  }
  memset(memory, 0, sizeof(memory));
  memcpy(memory + CHIP8_STARTINGPOINT, rom, i);
}

void chip8_reset(void)
{
  static const unsigned char font_8[40] =
  {
    0xf9, 0x99, 0xf2, 0x62, 0x27, 0xf1, 0xf8, 0xff, 0x1f, 0x1f,
    0x99, 0xf1, 0x1f, 0x8f, 0x1f, 0xf8, 0xf9, 0xff, 0x12, 0x44,
    0xf9, 0xf9, 0xff, 0x9f, 0x1f, 0xf9, 0xf9, 0x9e, 0x9e, 0x9e,
    0xf8, 0x88, 0xfe, 0x99, 0x9e, 0xf8, 0xf8, 0xff, 0x8f, 0x88
  };
  static const unsigned char font_s[160] =
  {
    0xff, 0xff, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xff, 0xff,
    0x18, 0x78, 0x78, 0x18, 0x18, 0x18, 0x18, 0x18, 0xff, 0xff,
    0xff, 0xff, 0x03, 0x03, 0xff, 0xff, 0xc0, 0xc0, 0xff, 0xff,
    0xff, 0xff, 0x03, 0x03, 0xff, 0xff, 0x03, 0x03, 0xff, 0xff,
    0xc3, 0xc3, 0xc3, 0xc3, 0xff, 0xff, 0x03, 0x03, 0x03, 0x03,
    0xff, 0xff, 0xc0, 0xc0, 0xff, 0xff, 0x03, 0x03, 0xff, 0xff,
    0xff, 0xff, 0xc0, 0xc0, 0xff, 0xff, 0xc3, 0xc3, 0xff, 0xff,
    0xff, 0xff, 0x03, 0x03, 0x06, 0x0c, 0x18, 0x18, 0x18, 0x18,
    0xff, 0xff, 0xc3, 0xc3, 0xff, 0xff, 0xc3, 0xc3, 0xff, 0xff,
    0xff, 0xff, 0xc3, 0xc3, 0xff, 0xff, 0x03, 0x03, 0xff, 0xff,
    0x7e, 0xff, 0xc3, 0xc3, 0xc3, 0xff, 0xff, 0xc3, 0xc3, 0xc3,
    0xfc, 0xfc, 0xc3, 0xc3, 0xfc, 0xfc, 0xc3, 0xc3, 0xfc, 0xfc,
    0x3c, 0xff, 0xc3, 0xc0, 0xc0, 0xc0, 0xc0, 0xc3, 0xff, 0x3c,
    0xfc, 0xfe, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xfe, 0xfc,
    0xff, 0xff, 0xc0, 0xc0, 0xff, 0xff, 0xc0, 0xc0, 0xff, 0xff,
    0xff, 0xff, 0xc0, 0xc0, 0xff, 0xff, 0xc0, 0xc0, 0xc0, 0xc0
  };
  unsigned int i;

  for (i = 0; i < sizeof(font_8); ++i)
  {
    MEM_STORE(FONT_START_8 + (i * 2)    ,                 (font_8[i] &  0xf0));
    MEM_STORE(FONT_START_8 + (i * 2) + 1, (unsigned char) (font_8[i] <<    4));
  }
  for (i = 0; i < sizeof(font_s); ++i)
  {
    MEM_STORE(FONT_START_S + (i    )    ,                 (font_s[i]        ));
  }

  I            = 0;
  PC           = CHIP8_STARTINGPOINT;
  SP           = 0;
  delay_timer  = 0;
  sound_timer  = 0;
  video_update = 1;
  memset(V, 0, sizeof(V));
  memset(screen, 0, sizeof(screen));
}
