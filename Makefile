CFLAGS := -Os -Wall -Wextra -pedantic
LDFLAGS := -Wl,-O1,--as-needed,--strip-all

.PHONY:	all check test clean
chip8-ncurses:	LDLIBS := -lncurses
chip8-test:	LDLIBS := -lcheck -lm -lrt -lsubunit -pthread
chip8-win32:	LDLIBS := -lcomdlg32 -lgdi32

all:	chip8-ncurses

chip8-ncurses:	chip8-ncurses.o core.o

chip8-test:	chip8-test.o

chip8-win32:	chip8-win32.o core.o

chip8-ncurses.o:	chip8-ncurses.c core.h

chip8-test.o:	chip8-test.c core.c core.h

chip8-win32.o:	chip8-win32.c core.h

core.o:	core.c core.h

check test:	chip8-test
	./$<

clean:
	$(RM) chip8-ncurses chip8-ncurses.o chip8-test chip8-test.o chip8-win32 chip8-win32.o core.o
