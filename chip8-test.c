#include <stdlib.h>

#include <check.h>

#include "core.c"

static void chip8_op(const unsigned int opcode)
{
  memory[0] = (opcode >> 8) & 0xff;
  memory[1] = (opcode     ) & 0xff;
  memory[2] = 0;
  memory[3] = 0;
  PC = 0;
  chip8_exec();
}

START_TEST(test_defines)
{
  ck_assert(CHIP8_MEMORYSIZE % 2 == 0);
  ck_assert(CHIP8_MEMORYSIZE > 0);
  ck_assert(CHIP8_RESOLUTION_X % 2 == 0);
  ck_assert(CHIP8_RESOLUTION_X > 0);
  ck_assert(CHIP8_RESOLUTION_Y % 2 == 0);
  ck_assert(CHIP8_RESOLUTION_Y > 0);
  ck_assert(CHIP8_SCREENSIZE / CHIP8_RESOLUTION_X == CHIP8_RESOLUTION_Y);
  ck_assert(CHIP8_STACKSIZE % 2 == 0);
  ck_assert(CHIP8_STACKSIZE > 0);
  ck_assert(CHIP8_STARTINGPOINT < CHIP8_MEMORYSIZE);
  ck_assert(CHIP8_STARTINGPOINT >= 0);
}
END_TEST

START_TEST(test_exec)
{
  PC = 1;
  delay_timer = 2;
  sound_timer = 2;
  video_update = 1;
  memory[1] = 0x10;
  memory[2] = 0x01;
  chip8_exec();
  ck_assert_int_eq(PC, 1);
  ck_assert_int_eq(delay_timer, 1);
  ck_assert_int_eq(sound_timer, 1);
  ck_assert_int_eq(video_update, 0);
  chip8_exec();
  chip8_exec();
  ck_assert_int_eq(delay_timer, 0);
  ck_assert_int_eq(sound_timer, 0);
}
END_TEST

START_TEST(test_framebuffer)
{
  ck_assert(chip8_framebuffer() != NULL);
}
END_TEST

START_TEST(test_init)
{
  const unsigned char rom = 0xff;
  memory[CHIP8_STARTINGPOINT] = 0;
  chip8_init(&rom, 1);
  ck_assert_int_eq(memory[CHIP8_STARTINGPOINT], rom);
}
END_TEST

START_TEST(test_op_0000)
{
  chip8_op(0x0000);
  ck_assert_int_eq(PC, 0);
}
END_TEST

START_TEST(test_op_00cn)
{
  screen[0] = 1;
  screen[CHIP8_RESOLUTION_X / 8] = 0;
  chip8_op(0x00c0);
  ck_assert_int_eq(screen[0], 1);
  ck_assert_int_eq(screen[CHIP8_RESOLUTION_X / 8], 0);
  chip8_op(0x00c3);
  ck_assert_int_eq(screen[3], 0);
  ck_assert_int_eq(screen[CHIP8_RESOLUTION_X / 8 * 3], 1);
}
END_TEST

START_TEST(test_op_00e0)
{
  screen[3] = 1;
  chip8_op(0x00e0);
  ck_assert_int_eq(screen[3], 0);
}
END_TEST

START_TEST(test_op_00ee)
{
  const unsigned int old_sp = SP;
  chip8_op(0x2111);
  ck_assert_int_eq(PC, 0x111);
  ck_assert_int_ne(SP, old_sp);
  chip8_op(0x00ee);
  ck_assert_int_eq(PC, 2);
  ck_assert_int_eq(SP, old_sp);
}
END_TEST

START_TEST(test_op_00fb)
{
  screen[0] = 0xff;
  screen[1] = 0;
  chip8_op(0x00fb);
  ck_assert_int_eq(screen[0], 0x0f);
  ck_assert_int_eq(screen[1], 0xf0);
}
END_TEST

START_TEST(test_op_00fc)
{
  screen[0] = 0x0f;
  screen[1] = 0xf0;
  chip8_op(0x00fc);
  ck_assert_int_eq(screen[0], 0xff);
  ck_assert_int_eq(screen[1], 0);
}
END_TEST

START_TEST(test_op_00fd)
{
  memory[CHIP8_STARTINGPOINT] = 0;
  memory[CHIP8_STARTINGPOINT + 1] = 0;
  chip8_op(0x00fd);
  ck_assert_int_eq(PC, CHIP8_STARTINGPOINT);
}
END_TEST

START_TEST(test_op_1nnn)
{
  memory[0x111] = 0;
  memory[0x112] = 0;
  chip8_op(0x1111);
  ck_assert_int_eq(PC, 0x111);
}
END_TEST

START_TEST(test_op_2nnn)
{
  SP = 0;
  chip8_op(0x2111);
  ck_assert_int_eq(PC, 0x111);
  ck_assert_int_eq(SP, 1);
  SP = CHIP8_STACKSIZE - 1;
  chip8_op(0x2222);
  ck_assert_int_eq(PC, 0x222);
  ck_assert_int_eq(SP, 0);
}
END_TEST

START_TEST(test_op_3xnn)
{
  V[0] = 0x11;
  memory[4] = 0;
  memory[5] = 0;
  chip8_op(0x3000);
  ck_assert_int_eq(PC, 2);
  chip8_op(0x3011);
  ck_assert_int_eq(PC, 4);
}
END_TEST

START_TEST(test_op_4xnn)
{
  V[0] = 0x11;
  memory[4] = 0;
  memory[5] = 0;
  chip8_op(0x4000);
  ck_assert_int_eq(PC, 4);
  chip8_op(0x4011);
  ck_assert_int_eq(PC, 2);
}
END_TEST

START_TEST(test_op_5xy0)
{
  V[0] = 0;
  V[1] = 0x11;
  V[2] = 0x11;
  memory[4] = 0;
  memory[5] = 0;
  chip8_op(0x5010);
  ck_assert_int_eq(PC, 2);
  chip8_op(0x5110);
  ck_assert_int_eq(PC, 4);
}
END_TEST

START_TEST(test_op_6xnn)
{
  V[9] = 0;
  chip8_op(0x6911);
  ck_assert_int_eq(V[9], 0x11);
}
END_TEST

START_TEST(test_op_7xnn)
{
  V[0] = 0x10;
  chip8_op(0x70fe);
  ck_assert_int_eq(V[0], 0x0e);
}
END_TEST

START_TEST(test_op_8xy0)
{
  V[0] = 0;
  V[1] = 9;
  chip8_op(0x8010);
  ck_assert_int_eq(V[0], 9);
  ck_assert_int_eq(V[1], 9);
}
END_TEST

START_TEST(test_op_8xy1)
{
  V[1] = 0xa0;
  V[2] = 0x0b;
  chip8_op(0x8121);
  ck_assert_int_eq(V[1], 0xab);
  ck_assert_int_eq(V[2], 0x0b);
}
END_TEST

START_TEST(test_op_8xy2)
{
  V[2] = 0x1f;
  V[3] = 0xf0;
  chip8_op(0x8232);
  ck_assert_int_eq(V[2], 0x10);
  ck_assert_int_eq(V[3], 0xf0);
}
END_TEST

START_TEST(test_op_8xy3)
{
  V[3] = 0x1f;
  V[4] = 0xf0;
  chip8_op(0x8343);
  ck_assert_int_eq(V[3], 0xef);
  ck_assert_int_eq(V[4], 0xf0);
}
END_TEST

START_TEST(test_op_8xy4)
{
  V[4] = 0x1f;
  V[5] = 0xf0;
  V[15] = 0;
  chip8_op(0x8454);
  ck_assert_int_eq(V[4], 0x0f);
  ck_assert_int_eq(V[5], 0xf0);
  ck_assert_int_eq(V[15], 1);
  chip8_op(0x84f4);
  ck_assert_int_eq(V[4], 0x10);
  ck_assert_int_eq(V[15], 0);
}
END_TEST

START_TEST(test_op_8xy5)
{
  V[5] = 0x1f;
  V[6] = 0xf0;
  V[15] = 0;
  chip8_op(0x8565);
  ck_assert_int_eq(V[5], 0x2f);
  ck_assert_int_eq(V[6], 0xf0);
  ck_assert_int_eq(V[15], 0);
  chip8_op(0x85f5);
  ck_assert_int_eq(V[5], 0x2f);
  ck_assert_int_eq(V[15], 1);
}
END_TEST

START_TEST(test_op_8x06)
{
  V[6] = 0xfe;
  V[7] = 0xfe;
  V[15] = 1;
  chip8_op(0x8676);
  ck_assert_int_eq(V[6], 0x7f);
  ck_assert_int_eq(V[7], 0xfe);
  ck_assert_int_eq(V[15], 0);
  chip8_op(0x8666);
  ck_assert_int_eq(V[6], 0x3f);
  ck_assert_int_eq(V[15], 1);
  chip8_op(0x8ff6);
  ck_assert_int_eq(V[15], 1);
}
END_TEST

START_TEST(test_op_8xy7)
{
  V[7] = 0xf0;
  V[8] = 0x1f;
  V[15] = 1;
  chip8_op(0x8787);
  ck_assert_int_eq(V[7], 0x2f);
  ck_assert_int_eq(V[8], 0x1f);
  ck_assert_int_eq(V[15], 0);
  chip8_op(0x8ff7);
  ck_assert_int_eq(V[15], 1);
}
END_TEST

START_TEST(test_op_8x0e)
{
  V[8] = 0x7f;
  V[9] = 0x7f;
  V[15] = 1;
  chip8_op(0x889e);
  ck_assert_int_eq(V[8], 0xfe);
  ck_assert_int_eq(V[9], 0x7f);
  ck_assert_int_eq(V[15], 0);
  chip8_op(0x888e);
  ck_assert_int_eq(V[8], 0xfc);
  ck_assert_int_eq(V[15], 1);
  chip8_op(0x8ffe);
  ck_assert_int_eq(V[15], 0);
}
END_TEST

START_TEST(test_op_9xy0)
{
  V[0] = 0;
  V[1] = 1;
  memory[4] = 0;
  memory[5] = 0;
  chip8_op(0x9000);
  ck_assert_int_eq(PC, 2);
  chip8_op(0x9010);
  ck_assert_int_eq(PC, 4);
}
END_TEST

START_TEST(test_op_annn)
{
  I = 0;
  chip8_op(0xa111);
  ck_assert_int_eq(I, 0x111);
}
END_TEST

START_TEST(test_op_bnnn)
{
  V[0] = 1;
  chip8_op(0xb111);
  ck_assert_int_eq(PC, 0x112);
}
END_TEST

START_TEST(test_op_cxnn)
{
  V[1] = 0;
  chip8_op(0xc1a0);
  ck_assert_int_eq(V[1], 0xa0);
}
END_TEST

START_TEST(test_op_dxyn)
{
  I = 0;
  V[0] = 0;
  V[15] = 1;
  chip8_op(0x00e0);
  chip8_op(0xd001);
  ck_assert_int_eq(screen[0], 0xd0);
  ck_assert_int_eq(V[15], 0);
  chip8_op(0xd001);
  ck_assert_int_eq(screen[0], 0);
  ck_assert_int_eq(V[15], 1);
  V[1] = 8;
  chip8_op(0xd01f);
  ck_assert_int_eq(screen[CHIP8_RESOLUTION_X], 0xd0);
  ck_assert_int_eq(V[15], 0);
}
END_TEST

START_TEST(test_op_ex9e)
{
  V[0] = 0;
  memory[4] = 0;
  memory[5] = 0;
  chip8_op(0xe09e);
  ck_assert_int_eq(PC, 2);
  V[0] = 0xff;
  chip8_op(0xe09e);
  ck_assert_int_eq(PC, 4);
}
END_TEST

START_TEST(test_op_exa1)
{
  V[0] = 0;
  memory[4] = 0;
  memory[5] = 0;
  chip8_op(0xe0a1);
  ck_assert_int_eq(PC, 4);
  V[0] = 0xff;
  chip8_op(0xe0a1);
  ck_assert_int_eq(PC, 2);
}
END_TEST

START_TEST(test_op_fx07)
{
  delay_timer = 7;
  V[0] = 0;
  chip8_op(0xf007);
  ck_assert_int_eq(delay_timer, 6);
  ck_assert_int_eq(V[0], 7);
}
END_TEST

START_TEST(test_op_fx0a)
{
  V[0] = 0;
  chip8_op(0xf00a);
  ck_assert_int_eq(PC, 0);
  V[0] = 0xff;
}
END_TEST

START_TEST(test_op_fx15)
{
  delay_timer = 0;
  V[0] = 7;
  chip8_op(0xf015);
  ck_assert_int_eq(delay_timer, 6);
  ck_assert_int_eq(V[0], 7);
}

END_TEST

START_TEST(test_op_fx18)
{
  sound_timer = 0;
  V[0] = 7;
  chip8_op(0xf018);
  ck_assert_int_eq(sound_timer, 6);
  ck_assert_int_eq(V[0], 7);
}
END_TEST

START_TEST(test_op_fx1e)
{
  I = 0xffe;
  V[0] = 1;
  V[15] = 0;
  chip8_op(0xf01e);
  ck_assert_int_eq(I, 0xfff);
  ck_assert_int_eq(V[15], 0);
#if CHIP8_QUIRKS
  chip8_op(0xf01e);
  ck_assert_int_eq(V[15], 1);
#endif
}
END_TEST

START_TEST(test_op_fx29)
{
  I = 0;
  V[0] = 0xff;
  chip8_op(0xf029);
  ck_assert(I > FONT_START_8);
  ck_assert(I < CHIP8_MEMORYSIZE);
}
END_TEST

START_TEST(test_op_fx30)
{
  I = 0;
  V[0] = 0xff;
  chip8_op(0xf030);
  ck_assert(I > FONT_START_S);
  ck_assert(I < CHIP8_MEMORYSIZE);
}
END_TEST

START_TEST(test_op_fx33)
{
  I = 4;
  V[0] = 123;
  memory[4] = 0;
  memory[5] = 0;
  memory[6] = 0;
  chip8_op(0xf033);
  ck_assert_int_eq(memory[4], 1);
  ck_assert_int_eq(memory[5], 2);
  ck_assert_int_eq(memory[6], 3);
}
END_TEST

START_TEST(test_op_fx55)
{
  I = 4;
  V[0] = 1;
  memory[4] = 0;
  chip8_op(0xf055);
#if CHIP8_QUIRKS
  ck_assert_int_eq(I, 4);
#else
  ck_assert_int_eq(I, 5);
#endif
  ck_assert_int_eq(V[0], 1);
  ck_assert_int_eq(memory[4], 1);
}
END_TEST

START_TEST(test_op_fx65)
{
  I = 4;
  V[0] = 0;
  memory[4] = 1;
  chip8_op(0xf065);
#if CHIP8_QUIRKS
  ck_assert_int_eq(I, 4);
#else
  ck_assert_int_eq(I, 5);
#endif
  ck_assert_int_eq(V[0], 1);
  ck_assert_int_eq(memory[4], 1);
}
END_TEST

START_TEST(test_op_fx75)
{
  HP48[0] = 0;
  HP48[1] = 0;
  V[0] = 1;
  V[1] = 2;
  chip8_op(0xf175);
  ck_assert_int_eq(HP48[0], 1);
  ck_assert_int_eq(HP48[1], 2);
  ck_assert_int_eq(V[0], 1);
  ck_assert_int_eq(V[1], 2);
}
END_TEST

START_TEST(test_op_fx85)
{
  HP48[0] = 1;
  HP48[1] = 2;
  V[0] = 0;
  V[1] = 0;
  chip8_op(0xf185);
  ck_assert_int_eq(HP48[0], 1);
  ck_assert_int_eq(HP48[1], 2);
  ck_assert_int_eq(V[0], 1);
  ck_assert_int_eq(V[1], 2);
}
END_TEST

START_TEST(test_reset)
{
  PC = !CHIP8_STARTINGPOINT;
  chip8_reset();
  ck_assert_int_eq(PC, CHIP8_STARTINGPOINT);
}
END_TEST

static Suite *chip8_suite(void)
{
  Suite *s;
  TCase *tc_core;

  s = suite_create("Chip8");
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_defines);
  tcase_add_test(tc_core, test_exec);
  tcase_add_test(tc_core, test_framebuffer);
  tcase_add_test(tc_core, test_init);
  tcase_add_test(tc_core, test_op_0000);
  tcase_add_test(tc_core, test_op_00cn);
  tcase_add_test(tc_core, test_op_00e0);
  tcase_add_test(tc_core, test_op_00ee);
  tcase_add_test(tc_core, test_op_00fb);
  tcase_add_test(tc_core, test_op_00fc);
  tcase_add_test(tc_core, test_op_00fd);
  tcase_add_test(tc_core, test_op_1nnn);
  tcase_add_test(tc_core, test_op_2nnn);
  tcase_add_test(tc_core, test_op_3xnn);
  tcase_add_test(tc_core, test_op_4xnn);
  tcase_add_test(tc_core, test_op_5xy0);
  tcase_add_test(tc_core, test_op_6xnn);
  tcase_add_test(tc_core, test_op_7xnn);
  tcase_add_test(tc_core, test_op_8x06);
  tcase_add_test(tc_core, test_op_8x0e);
  tcase_add_test(tc_core, test_op_8xy0);
  tcase_add_test(tc_core, test_op_8xy1);
  tcase_add_test(tc_core, test_op_8xy2);
  tcase_add_test(tc_core, test_op_8xy3);
  tcase_add_test(tc_core, test_op_8xy4);
  tcase_add_test(tc_core, test_op_8xy5);
  tcase_add_test(tc_core, test_op_8xy7);
  tcase_add_test(tc_core, test_op_9xy0);
  tcase_add_test(tc_core, test_op_annn);
  tcase_add_test(tc_core, test_op_bnnn);
  tcase_add_test(tc_core, test_op_cxnn);
  tcase_add_test(tc_core, test_op_dxyn);
  tcase_add_test(tc_core, test_op_ex9e);
  tcase_add_test(tc_core, test_op_exa1);
  tcase_add_test(tc_core, test_op_fx07);
  tcase_add_test(tc_core, test_op_fx0a);
  tcase_add_test(tc_core, test_op_fx15);
  tcase_add_test(tc_core, test_op_fx18);
  tcase_add_test(tc_core, test_op_fx1e);
  tcase_add_test(tc_core, test_op_fx29);
  tcase_add_test(tc_core, test_op_fx30);
  tcase_add_test(tc_core, test_op_fx33);
  tcase_add_test(tc_core, test_op_fx55);
  tcase_add_test(tc_core, test_op_fx65);
  tcase_add_test(tc_core, test_op_fx75);
  tcase_add_test(tc_core, test_op_fx85);
  tcase_add_test(tc_core, test_reset);
  suite_add_tcase(s, tc_core);

  return s;
}

void chip8_buzz(void)
{
  ck_assert(sound_timer <= 0xff);
}

void chip8_draw(const unsigned char *framebuffer)
{
  ck_assert(framebuffer != NULL);
}

unsigned char chip8_key(void)
{
  return 0xff;
}

unsigned char chip8_random(void)
{
  return 0xff;
}

int main(void)
{
  SRunner *sr;
  int number_failed;

  sr = srunner_create(chip8_suite());
  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return number_failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
